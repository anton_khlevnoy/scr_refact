﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Winforms = System.Windows.Forms;

namespace Scr
{
    public class Refact
    {
        string pattern;
        string oldText;
        string newText;

        public bool CommentInsteadDelete { get; set; }

        List<string> pathesToAllFiles = new List<string>();
        List<string> pathesToFilesContainigPattern = new List<string>();
        //List<string> pathesToTempFiles = new List<string>();
        List<string> newPathesToFilesContainigPattern = new List<string>();
        List<string> fileNamesContainigPattern = new List<string>();
        List<string> newFileNamesContainigPattern = new List<string>();

        public Refact(string path, string pattern, string oldText, string newText, string searchFilesPattern)
        {
            this.pattern = pattern;
            this.oldText = oldText;
            this.newText = newText;
            ReturnPathes(path, pathesToAllFiles, searchFilesPattern);
            ReturnPathesContainingPattern(pathesToAllFiles, pathesToFilesContainigPattern, fileNamesContainigPattern, pattern);
        }


        // меняем слово или его чать по шаблону (остальная строка нетронута)
        public void ReplaceTextByPatternOneWord(int mode)
        {
            switch (mode)
            {
                case 1:
                    ReplaceTextByPatternOneWord(oldText, oldText, newText);
                    break;
                case 2:
                    ReplaceTextByPatternOneWord(pattern, oldText, newText);
                    break;
                default:
                    ReplaceTextByPatternOneWord(oldText, oldText, newText);
                    break;
            }
        }
        void ReplaceTextByPatternOneWord(string pattern, string oldText, string newText)
        {
            foreach (var file in pathesToFilesContainigPattern)
            {
                string text = File.ReadAllText(file);
                string[] textLines = text.Split(new char[] { '\n' });
                List<string> theNewText = new List<string>();
                foreach (var textLine in textLines)
                {
                    if (TextContainsPattern(textLine, pattern, 0))
                    {
                        List<string> newTextLine = new List<string>();
                        string[] words = textLine.Split(new char[] { ' ' });
                        foreach (var word in words)
                        {
                            if (TextContainsPattern(word, pattern, 0))
                            {

                                string modifiedWord = ReplaceTextByPatternOneWord(word, oldText, newText, 0);

                                newTextLine.Add(modifiedWord);
                            }
                            else
                            {
                                newTextLine.Add(word);
                            }
                        }
                        theNewText.Add(string.Join(" ", newTextLine));
                        if (CommentInsteadDelete)
                        {
                            theNewText.Add("--" + textLine);
                        }
                    }
                    else
                    {
                        theNewText.Add(textLine);
                    }
                }
                File.WriteAllText(file, string.Join("\n", theNewText));
            }
        }

        // mode = 0 - убирать квадратные скобки по необходимости, mode = 1 - убирать скобки всегда
        string ReplaceTextByPatternOneWord(string existingText, string oldText, string newText, int mode)
        {
            string modifiedWord = "";

            if (newText.Contains('.') && oldText.Contains('.'))
            {
                string[] newWordParts = newText.Split(new char[] { '.' });
                string newWordText = "";

                if (existingText.Contains("[") && existingText.Contains("]"))
                {
                    for (int i = 0; i < newWordParts.Count(); i++)
                    {
                        if (i == 0)
                        {
                            newWordText = $"[{newWordParts[i]}]";
                        }
                        else
                        {
                            newWordText += $".[{newWordParts[i]}";
                        }                        
                    }
                }
                else
                {
                    newWordText = newText;
                }
                modifiedWord = existingText.Replace(oldText, newWordText);              
            }
            else 
            {
                modifiedWord = existingText.Replace(oldText, newText);
            }

            if (mode == 1)
            {
                modifiedWord = modifiedWord.Replace("[", "").Replace("]", "");
            }          

            return modifiedWord;
        }

        // меняем строку по шаблону (по части строки)
        public void ReplaceTextByPatternFullLine()
        {
            ReplaceTextByPatternFullLine(pattern, newText);
        }
        void ReplaceTextByPatternFullLine(string pettern, string newText)
        {
            foreach (var file in pathesToFilesContainigPattern)
            {
                string text = File.ReadAllText(file);
                string[] textLines = text.Split(new char[] { '\n' });
                List<string> theNewText = new List<string>();
                foreach (var textLine in textLines)
                {
                    if (TextContainsPattern(textLine, pettern, 0))
                    {
                        if (!string.IsNullOrEmpty(newText))
                        {
                            string modifiedTextLine = newText;
                            // проверим запятую перед словом, чтобы добавить, если она была (для селекта)
                            if (textLine.Trim().Substring(0, 1) == ",")
                            {
                                modifiedTextLine = "," + newText;
                            }

                            // считаем количество табов и пробелов в начале строки
                            int numTabs = 0;
                            int numSpaces = 0;
                            foreach (char c in textLine)
                            {
                                if (c == '\t')
                                {
                                    numTabs++;
                                }
                                else if (c == ' ')
                                {
                                    numSpaces++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            if (numTabs != 0)
                            {
                                for (int i = 0; i < numTabs; i++)
                                {
                                    modifiedTextLine = "\t" + modifiedTextLine;
                                }
                            }
                            if (numSpaces != 0)
                            {
                                for (int i = 0; i < numSpaces; i++)
                                {
                                    modifiedTextLine = " " + modifiedTextLine;
                                }
                            }
                            theNewText.Add(modifiedTextLine);
                        }
                        
                        if (CommentInsteadDelete)
                        {
                            theNewText.Add("--" + textLine);
                        }
                    }
                    else
                    {
                        theNewText.Add(textLine);
                    }
                }
                File.WriteAllText(file, string.Join("\n", theNewText));
            }
        }


        // меняем строку по шаблону (по полному сооветствию слова)
        public void ReplaceTextByWordFullLine()
        {
            ReplaceTextByWordFullLine(pattern, newText);
        }
        void ReplaceTextByWordFullLine(string pattern, string newText)
        {
            foreach (var file in pathesToFilesContainigPattern)
            {
                string text = File.ReadAllText(file);
                string[] textLines = text.Split(new char[] { '\n' });
                List<string> theNewText = new List<string>();
                foreach (var textLine in textLines)
                {
                    if (TextContainsPattern(textLine, pattern, 0))
                    {
                        string modifiedTextLine = newText;
                        bool didNotChange = true;
                        string[] words = textLine.Split(new char[] { ' ' });
                        foreach (var word in words)
                        {
                            if (word.Trim().ToLower().Replace(",", "").Replace("[", "").Replace("]", "") == pattern.ToLower().Replace("[", "").Replace("]", ""))
                            {
                                if (!string.IsNullOrEmpty(newText))
                                {
                                    // проверим запятую перед словом, чтобы добавить, если она была (для селекта)
                                    if (textLine.Trim().StartsWith(","))
                                    {
                                        modifiedTextLine = "," + newText;
                                    }

                                    // считаем количество табов и пробелов в начале строки
                                    int numTabs = 0;
                                    int numSpaces = 0;
                                    foreach (char c in textLine)
                                    {
                                        if (c == '\t')
                                        {
                                            numTabs++;
                                        }
                                        else if (c == ' ')
                                        {
                                            numSpaces++;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    if (numTabs != 0)
                                    {
                                        for (int i = 0; i < numTabs; i++)
                                        {
                                            modifiedTextLine = "\t" + modifiedTextLine;
                                        }
                                    }
                                    if (numSpaces != 0)
                                    {
                                        for (int i = 0; i < numSpaces; i++)
                                        {
                                            modifiedTextLine = " " + modifiedTextLine;
                                        }
                                    }
                                    theNewText.Add(modifiedTextLine);
                                }                               
                                if (CommentInsteadDelete)
                                {
                                    theNewText.Add("--" + textLine);
                                }
                                didNotChange = false;
                                break;
                            }

                        }
                        if (didNotChange)
                        {
                            theNewText.Add(textLine);
                        }
                    }
                    else
                    {
                        theNewText.Add(textLine);
                    }
                }
                File.WriteAllText(file, string.Join("\n", theNewText));
            }
        }

        // вставляем строку до или после строки по шаблону (по части строки): position = 0 - до, position = 1 - после, mode = 0 - один раз после первого вхождения, mode = 1 - после каждого вхождения
        public void InsertBeforeTextByPatternFullLineOnce()
        {
            InsertTextByPatternFullLine(newText, 0, 0);
        }
        public void InsertAfterTextByPatternFullLineOnce()
        {
            InsertTextByPatternFullLine(newText, 1, 0);
        }
        public void InsertBeforeTextByPatternFullLineAll()
        {
            InsertTextByPatternFullLine(newText, 0, 1);
        }
        public void InsertAfterTextByPatternFullLineAll()
        {
            InsertTextByPatternFullLine(newText, 1, 1);
        }
        void InsertTextByPatternFullLine(string newText, int position, int mode)
        {
            foreach (var file in pathesToFilesContainigPattern)
            {
                string text = File.ReadAllText(file);
                string[] textLines = text.Split(new char[] { '\n' });
                List<string> theNewText = new List<string>();
                int ind = 0;
                foreach (var textLine in textLines)
                {
                    
                    if (TextContainsPattern(textLine, oldText, 0))
                    {
                        if (position == 0)
                        {
                            theNewText.Add(newText);
                            ind++;
                        }

                        if (mode == 0)
                        {
                            theNewText.Add(textLine);
                            ind++;
                            break;
                        }
                        else
                        {
                            theNewText.Add(textLine);
                        }

                        if (position == 1 && mode != 0)
                        {
                            theNewText.Add(newText);
                            ind++;
                        }
                    }
                    else
                    {
                        theNewText.Add(textLine);
                        ind++;
                    }

                }

                if (mode == 0)
                {
                    theNewText.Add(newText);
                    for (int i = ind; i < textLines.Length; i++)
                    {
                        theNewText.Add(textLines[i]);
                    }
                }

                File.WriteAllText(file, string.Join("\n", theNewText));
            }
        }


        // удаляем слово или его чать по шаблону (остальная строка нетронута)
        public void DeleteTextByPatternOneWord()
        {
            ReplaceTextByPatternOneWord(pattern, oldText, "");
        }

        // удвляем строку по шаблону (по части строки)
        public void DeleteTextByPatternFullLine()
        {
            ReplaceTextByPatternFullLine(pattern, "");
        }

        // удаляем по шаблону (по полному сооветствию слова)
        public void DeleteTextByWordFullLine()
        {
            ReplaceTextByWordFullLine(pattern, "");
        }


        public void RenameFilesAndSqlproj(string pathToSqlproj)
        {
            RenameFilesAndSqlproj(pathToSqlproj, pathesToFilesContainigPattern, newPathesToFilesContainigPattern, fileNamesContainigPattern, newFileNamesContainigPattern, oldText, newText);
        }
        void RenameFilesAndSqlproj (string pathToSqlproj, List<string> oldPathesToFiles, List<string> newPathesToFiles, List<string> oldNamesOfFiles, List<string> newNamesOfFiles, string exText, string newText)
        {
            if (string.IsNullOrEmpty(pathToSqlproj))
            {
                Winforms.OpenFileDialog dialog = new Winforms.OpenFileDialog();
                dialog.Filter = "(*.sqlproj)| *.sqlproj";
                dialog.ShowDialog();
                if (string.IsNullOrEmpty(dialog.FileName))
                {
                    return;
                }
                pathToSqlproj = dialog.FileName;
            }
            string textSqlProj = "";

            if (File.Exists(pathToSqlproj))
            {
                textSqlProj = File.ReadAllText(pathToSqlproj);
            }         

            for (int i = 0; i < oldNamesOfFiles.Count; i++)
            {
                newNamesOfFiles.Add(ReplaceTextByPatternOneWord(oldNamesOfFiles[i], exText, newText, 1));
                textSqlProj = ReplaceTextByPatternOneWord(textSqlProj, exText, newText, 1);

                string pathBegin = oldPathesToFiles[i].Replace(oldNamesOfFiles[i], "");
                newPathesToFiles.Add(pathBegin + newNamesOfFiles[i]);

                try
                {
                    File.Move(oldPathesToFiles[i], newPathesToFiles[i]);
                }
                catch { MessageBox.Show($"Не удается переименовать файл: {oldPathesToFiles[i]} -> {newPathesToFiles[i]}"); }
            }

            if (File.Exists(pathToSqlproj))
            {
                try
                {
                    File.WriteAllText(pathToSqlproj, textSqlProj);
                }
                catch { MessageBox.Show("Не удается переписать .sqlproj"); }
            }         

        }







        public void GenerateScriptRenameTables(string pathToScript)
        {
            GenerateScriptRenameTables(pathToScript, pathesToFilesContainigPattern, pattern, oldText, newText);
        }
        void GenerateScriptRenameTables(string pathToScriptFolder, List<string> pathesToFilesContainigPattern, string pattern, string oldText, string newText)
        {

            if (pathesToFilesContainigPattern.Count == 0)
            {
                return;
            }

            if (!Directory.Exists(pathToScriptFolder)) { return; }

            string pathToScriptFile = pathToScriptFolder + "\\RefactScript.sql";

            string scriptText = "USE [CBI_DWH]\nGO\n\n";
         
            foreach (var file in pathesToFilesContainigPattern)
            {               
                string text = File.ReadAllText(file);

                if (TextContainsPattern(text, "create table", 0))
                {
                    string schemaNameOld = "";
                    string schemaNameNew = "";
                    string tableNameOld = "";
                    string tableNameNew = "";
                    string tablePKCNameOld = "";
                    string tablePKCNameNew = "";
                    string[] textLines = text.Split(new char[] { '\n' });
                    foreach (string textLine in textLines)
                    {
                        if (TextContainsPattern(textLine, pattern, 0))
                        {
                            string[] words = textLine.Split(new char[] { ' ' });
                            foreach (string word in words)
                            {
                                if (TextContainsPattern(word, pattern, 0))
                                {
                                    string newWord = word.Replace("]", "").Replace("[", "");
                                    if (TextContainsPattern(newWord, "(", 0))
                                    {
                                        newWord = newWord.Substring(0, newWord.IndexOf('('));
                                    }
                                  
                                    string[] wordParts = newWord.Split(new char[] { '.' });
                                    if (wordParts.Length == 2)
                                    {
                                        schemaNameOld = wordParts[0].Trim();
                                        tableNameOld = wordParts[1].Trim();
                                    }

                                    newWord = ReplaceTextByPatternOneWord(newWord, oldText, newText, 1);
                                    if (TextContainsPattern(newWord, "(", 0))
                                    {
                                        newWord = newWord.Substring(0, newWord.IndexOf('('));
                                    }
                                    string[] newWordParts = newWord.Split(new char[] { '.' });
                                    if (newWordParts.Length == 2)
                                    {
                                        schemaNameNew = newWordParts[0].Trim();
                                        tableNameNew = newWordParts[1].Trim();
                                    }
                                    break;
                                }
                            }                         
                        }


                        if (TextContainsPattern(textLine, "PKC_", 0) && !string.IsNullOrEmpty(tableNameOld) && !string.IsNullOrEmpty(tableNameNew))
                        {
                            string[] words = textLine.Split(new char[] { ' ' });
                            foreach (string word in words)
                            {
                                if (TextContainsPattern(word, "PKC_", 0))
                                {
                                    tablePKCNameOld = word.Replace("]", "").Replace("[", "");
                                    tablePKCNameNew = ReplaceTextByPatternOneWord(tablePKCNameOld, tableNameOld, tableNameNew, 0);
                                    break;
                                }
                            }
                        }


                    }

                    if (!string.IsNullOrEmpty(schemaNameOld) && !string.IsNullOrEmpty(schemaNameNew) && !string.IsNullOrEmpty(tableNameOld) && !string.IsNullOrEmpty(tableNameNew))
                    {
                        if (tableNameOld != tableNameNew)
                        {
                            scriptText += $"exec sp_rename '{schemaNameOld}.{tableNameOld}', '{tableNameNew}'\nGO\n\n";
                        }
                        
                        if (schemaNameOld != schemaNameNew)
                        {
                            scriptText += $"IF (NOT EXISTS (SELECT * FROM sys.schemas WHERE name = '{schemaNameNew}')) BEGIN EXEC ('CREATE SCHEMA [{schemaNameNew}] AUTHORIZATION [{schemaNameOld}]') END\n";
                            scriptText += $"alter schema {schemaNameNew} transfer {schemaNameOld}.{tableNameNew}\nGO\n\n";
                        }

                        if (tableNameOld != tableNameNew && !string.IsNullOrEmpty(tablePKCNameOld) && !string.IsNullOrEmpty(tablePKCNameNew))
                        {
                            scriptText += $"exec sp_rename '{schemaNameOld}.{tableNameNew}.{tablePKCNameOld}', '{tablePKCNameNew}'\nGO\n\n";
                        }
                    }


                } 
                
            }

            try
            {
                using (StreamWriter sw = File.AppendText(pathToScriptFile))
                {
                    sw.Write(scriptText);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); return; }
        }




        public void RenameProcViewFuncWithCopyFiles(string pathToScript)
        {
            RenameProcViewFuncWithCopyFiles(pathToScript, pathesToFilesContainigPattern, pattern, oldText, newText);
        }
        // переименовываем названия таблиц в функциях, процедурах и вьюхах при этом записываем их в темповую таблицу или меняем их в темповой если там уже есть, 
        // в скрипт отдельная процедура будет писать
        void RenameProcViewFuncWithCopyFiles(string pathToScriptFolder, List<string> pathesToFilesContainigPattern, string pattern, string oldText, string newText)
        {

            if (pathesToFilesContainigPattern.Count == 0)
            {
                return;
            }

            if (!Directory.Exists(pathToScriptFolder)) { return; }

            string dirTempFiles = pathToScriptFolder + "\\tempFiles";
            try
            {
                new DirectoryInfo(dirTempFiles).Create();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); return; }    

            foreach (var file in pathesToFilesContainigPattern)
            {
                string tempFilePathExisting = dirTempFiles + "\\" + file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);

                string scriptText = "";
                string text = "";

                if (File.Exists(tempFilePathExisting))
                {
                    text = File.ReadAllText(tempFilePathExisting);
                }
                else
                {
                    text = File.ReadAllText(file);
                }

                if (TextContainsPattern(text, pattern, 1))
                {
                    string tempFilePathAdd = "";
                    if (File.Exists(tempFilePathExisting))
                    {                       
                        string[] textLines = text.Split(new char[] { '\n' });
                        string dropComand = textLines[0] + "\n" + textLines[1];
                        string textWithoutDropCommand = text.Replace(dropComand, "");
                        text = dropComand + "\n" + ReplaceTextByPatternOneWord(textWithoutDropCommand, oldText, newText, 1);
                        scriptText += text + "\n" + "GO\n\n";
                    }
                    else if ((TextContainsPattern(text, "create procedure", 1) || TextContainsPattern(text, "create function", 1) || TextContainsPattern(text, "create view", 1)) 
                        && !TextContainsPattern(text, "create table", 0))
                    {

                        string objectType = "";
                        string objectName = "";
                        List<string> wordsOfScript = new List<string>();
                        string[] lines = text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string line in lines)
                        {
                            string[] words = line.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string word in words )
                            {
                                if (!word.Trim().StartsWith("-"))
                                {
                                    wordsOfScript.Add(word.Trim());
                                }
                            }
                        }
                        foreach (string wordOfScript in wordsOfScript)
                        {
                            if (wordOfScript == " ")
                            {
                                wordsOfScript.Remove(wordOfScript);
                            }
                        }
                        for (int i = 0; i < wordsOfScript.Count() - 2; i++)
                        {
                            if (wordsOfScript[i].ToLower() == "create")
                            {
                                objectType = wordsOfScript[i + 1];
                                objectName = wordsOfScript[i + 2];

                                if (objectType.ToLower() == "procedure" || objectType.ToLower() == "function" || objectType.ToLower() == "view")
                                {
                                    break;
                                }                               
                            }                          
                        }

                        text = $"/*AutoGenDropScriptLine*/ drop {objectType} {objectName}\nGO\n\n" + ReplaceTextByPatternOneWord(text, oldText, newText, 1);
                        scriptText += text + "\n" + "GO\n\n";
                        tempFilePathAdd = tempFilePathExisting;
                    }

                    if (!string.IsNullOrEmpty(tempFilePathAdd))
                    {
                        Scr.TextValues.pathesToTempFiles.Add(tempFilePathAdd);
                    }

                    if (File.Exists(tempFilePathExisting))
                    {
                        using (StreamWriter sw = new StreamWriter(tempFilePathExisting))
                        {
                            sw.Write(scriptText);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tempFilePathAdd))
                        {
                            try
                            {
                                File.Copy(file, tempFilePathAdd);
                                using (StreamWriter sw = new StreamWriter(tempFilePathAdd))
                                {
                                    sw.Write(scriptText);
                                }
                            }
                            catch (Exception ex) { MessageBox.Show(ex.Message); return; }
                        }                       
                    }
                }               
            }
        }


        // записываем в общий скрипт файлы из темповых и удаляем темповые
        public void CopyRenamedProcViewFuncTextIntoScript(string pathToScriptFolder)
        {
            if (!Directory.Exists(pathToScriptFolder)) { return; }

            string pathToScriptFile = pathToScriptFolder + "\\RefactScript.sql";
            string pathToTempFilesFolder = pathToScriptFolder + "\\tempFiles";
            string textScriptRenameProcViewFunc = "";

            if (!File.Exists(pathToScriptFile)) { File.Create(pathToScriptFile); }

            foreach (string tempFile in Scr.TextValues.pathesToTempFiles)
            {
                textScriptRenameProcViewFunc += File.ReadAllText(tempFile);
                //  File.Delete(tempFile);
            }

            try
            {
                using (StreamWriter sw = File.AppendText(pathToScriptFile))
                {
                    sw.Write(textScriptRenameProcViewFunc);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); return; }

            if (Directory.GetFiles(pathToTempFilesFolder).Length == 0)
            {
                try { Directory.Delete(pathToTempFilesFolder); }
                catch (Exception ex) { MessageBox.Show(ex.Message); return; }               
            }
        }



        



















        public void ShowAllFilesList (TextBox tb, string mode)
        {
            foreach (var path in pathesToAllFiles)
            {
                switch (mode)
                {
                    case "full":
                        tb.Text += path + "\n";
                        break;
                    case "names":
                        tb.Text += Path.GetFileName(path) + "\n";
                        break;
                    case "namesNoExtension":
                        tb.Text += Path.GetFileNameWithoutExtension(path) + "\n";
                        break;
                    default:
                        break;
                }
            }
        }

        public void ShowFilesContainPatternList(TextBox tb, string mode)
        {
            foreach (var path in pathesToFilesContainigPattern)
            {
                switch (mode)
                {
                    case "full":
                        tb.Text += path + "\n";
                        break;
                    case "names":
                        tb.Text += Path.GetFileName(path) + "\n";
                        break;
                    case "namesNoExtension":
                        tb.Text += Path.GetFileNameWithoutExtension(path) + "\n";
                        break;
                    default:
                        break;
                }
            }
        }


        void ReturnPathesContainingPattern(List<string> pathes, List<string> pathesPettern, List<string> namesList, string pattern)
        {
            foreach (var path in pathes)
            {
                if (!TextContainsPattern(path, @"bin\Debug", 0))
                {
                    string text = File.ReadAllText(path);

                    if (TextContainsPattern(text, pattern, 0) || TextContainsPattern(path.Substring(path.LastIndexOf('\\') + 1, path.Length - path.LastIndexOf('\\') - 1), pattern, 0))
                    {
                        pathesPettern.Add(path);
                        namesList.Add(path.Substring(path.LastIndexOf('\\') + 1, path.Length - path.LastIndexOf('\\') - 1));
                    }
                }               
            }
        }

        // ищем файлы в выбранной папке по паттерну
        void ReturnPathes(string directory, List<string> outList, string pattern)
        {
            foreach (var file in Directory.GetFiles(directory, pattern, SearchOption.AllDirectories))
            {
                outList.Add(file);
            }
        }

        // mode = 0 - смотрим по-старинке на все слова вместе, 1 - слова делятся по пробелам и смотрится текст на наличие двух слов независимо
        bool TextContainsPattern(string text, string pattern, int mode)
        {
            if (mode == 0)
            {
                if (text.ToLower().Replace("[", "").Replace("]", "").Contains(pattern.ToLower().Replace("[", "").Replace("]", "")))
                {
                    return true;
                }
            }

            string[] wordsPattern = pattern.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (wordsPattern.Count() == 1)
            {
                if (text.ToLower().Replace("[", "").Replace("]", "").Contains(pattern.ToLower().Replace("[", "").Replace("]", "")))
                {
                    return true;
                }
            }
            else
            {
                string[] lines = text.Split(new char[] { '\n' });
                for (int i = 0; i < lines.Count() - 2; i++)
                {
                    if (lines[i].ToLower().Replace("[", "").Replace("]", "").Contains(wordsPattern[0].ToLower().Replace("[", "").Replace("]", ""))
                        && (lines[i].ToLower().Replace("[", "").Replace("]", "").Contains(wordsPattern[1].ToLower().Replace("[", "").Replace("]", ""))
                        || lines[i + 1].ToLower().Replace("[", "").Replace("]", "").Contains(wordsPattern[1].ToLower().Replace("[", "").Replace("]", ""))
                        || lines[i + 2].ToLower().Replace("[", "").Replace("]", "").Contains(wordsPattern[1].ToLower().Replace("[", "").Replace("]", "")))
                        )
                    {
                        return true;
                    }
                }
            }

            return false;
        }

    }
}
