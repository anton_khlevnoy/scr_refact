﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scr
{
    class WhatToDo
    {
        public static List<string> listWhatToDo = new List<string>()
        {
            "1 Show files containing pattern",
            "2.1 Replace existing text by the new one (word or part only)",
            "2.2 Replace full line with existing text by the new one (by pattern)",
            "2.3 Replace full line with existing text by the new one (full word pattern)",
            "2.4 Replace part of the word by the new one (pattern != oldText, replaced by newText)",
            "3.1 Delete part of word in text (by pattern)",
            "3.2 Delete full line (by pattern)",
            "3.3 Delete full line (by word)",
            "4.1 Insert line before line with pattern once",
            "4.2 Insert line before line with pattern all times",
            "4.3 Insert line after line with pattern once",
            "4.4 Insert line after line with pattern all times",
            "5 Rename files and change sqlproj",
        };
    }
}
