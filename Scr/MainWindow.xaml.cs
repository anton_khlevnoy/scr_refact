﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Winforms = System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace Scr
{
    delegate void UpdateProgressBarDelegate(DependencyProperty dp, object value);

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BackgroundWorker worker;

        public MainWindow()
        {
            InitializeComponent();
            cbbx_WhatToDo.ItemsSource = WhatToDo.listWhatToDo;
            TextValues.initDialogPath = Winforms.Application.StartupPath.ToString();

            worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
        }

        void worker_DoWork (object sender, DoWorkEventArgs e)
        {
            UpdateProgressBarDelegate updProgress = new UpdateProgressBarDelegate(progress.SetValue);
            double progressValue = 0;
            string taskFileContent = File.ReadAllText(TextValues.refactFile);
            string[] taskLines = taskFileContent.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> tasks = new List<string>();
            foreach (string line in taskLines)
            {
                if (!line.Trim().StartsWith("#") && !string.IsNullOrEmpty(line.Trim()) && line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length == 6)
                {
                    tasks.Add(line);
                }
            }

            if (tasks.Count > 0)
            {
                string warningMessage = $"Найдено заданий: {tasks.Count}. Выполнить?";

                Winforms.DialogResult warningDialog = Winforms.MessageBox.Show(warningMessage, "Обнаружены задания", Winforms.MessageBoxButtons.OKCancel);
                if (warningDialog == Winforms.DialogResult.Cancel)
                {
                    return;
                }
            }

            else
            {
                MessageBox.Show($"Задиния не обнаружены");
                return;
            }

            int currentTask = 0;

            foreach (string task in tasks)
            {
                currentTask++;

                string[] vars = task.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (vars.Length != 6)
                {
                    MessageBox.Show($"Неверно задан формат задания: {task}, неверное число переменных");
                    continue;
                }
                string pathSql = (vars[0].Trim() == "#") ? "" : vars[0].Trim();
                string pattern = (vars[1].Trim() == "#") ? "" : vars[1].Trim();
                string oldText = (vars[2].Trim() == "#") ? "" : vars[2].Trim();
                string newText = (vars[3].Trim() == "#") ? "" : vars[3].Trim();
                string pathSqlproj = (vars[4].Trim() == "#") ? "" : vars[4].Trim();
                string mode = (vars[5].Trim() == "#") ? "" : vars[5].Trim();
                Refact refact = new Refact(pathSql, pattern, oldText, newText, "*.sql");
                switch (mode)
                {
                    case "1":
                        refact.ShowFilesContainPatternList(txtbox_sql_files, "namesNoExtension");
                        break;
                    case "2.1":
                        refact.ReplaceTextByPatternOneWord(1);
                        break;
                    case "2.2":
                        refact.ReplaceTextByPatternFullLine();
                        break;
                    case "2.3":
                        refact.ReplaceTextByWordFullLine();
                        break;
                    case "2.4":
                        refact.ReplaceTextByPatternOneWord(2);
                        break;
                    case "2.5":
                        refact.GenerateScriptRenameTables(TextValues.scriptFile);
                        refact.RenameProcViewFuncWithCopyFiles(TextValues.scriptFile);
                        if (currentTask == tasks.Count)
                        {
                            refact.CopyRenamedProcViewFuncTextIntoScript(TextValues.scriptFile);
                        }
                        break;
                    case "3.1":
                        refact.DeleteTextByPatternOneWord();
                        break;
                    case "3.2":
                        refact.DeleteTextByPatternFullLine();
                        break;
                    case "3.3":
                        refact.DeleteTextByWordFullLine();
                        break;
                    case "4.1":
                        refact.InsertBeforeTextByPatternFullLineOnce();
                        break;
                    case "4.2":
                        refact.InsertBeforeTextByPatternFullLineAll();
                        break;
                    case "4.3":
                        refact.InsertAfterTextByPatternFullLineOnce();
                        break;
                    case "4.4":
                        refact.InsertAfterTextByPatternFullLineAll();
                        break;
                    case "5":
                        refact.RenameFilesAndSqlproj(pathSqlproj);
                        break;
                    default:
                        refact.ShowFilesContainPatternList(txtbox_sql_files, "full");
                        break;
                }
                Dispatcher.Invoke(updProgress, new object[] { ProgressBar.ValueProperty, ++progressValue });

            }
            TextValues.refactFile = "";
        }



        private void btn_run_task_Button_Click(object sender, RoutedEventArgs e)
        {
            Winforms.OpenFileDialog dialog = new Winforms.OpenFileDialog();
            dialog.Filter = "(*.refact)| *.refact";
            dialog.ShowDialog();
            if (string.IsNullOrEmpty(dialog.FileName))
            {
                return;
            }
            TextValues.refactFile = dialog.FileName;

            string taskFileContent = File.ReadAllText(dialog.FileName);
            string[] taskLines = taskFileContent.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> tasks = new List<string>();
            foreach (string line in taskLines)
            {
                if (!line.Trim().StartsWith("#") && !string.IsNullOrEmpty(line.Trim()) && line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length == 6)
                {
                    tasks.Add(line);
                }
            }

            progress.Maximum = tasks.Count;
            progress.Value = 0;

            foreach (string task in tasks)
            {
                string[] vars = task.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string mode = (vars[5].Trim() == "#") ? "" : vars[5].Trim();
                if (mode == "2.5")
                {
                    using (var dlg = new Winforms.FolderBrowserDialog())
                    {
                        // путь к папке, в которой будет происходить поиск sql скриптов
                        if (Directory.Exists(TextValues.initDialogPath))
                        {
                            dlg.SelectedPath = TextValues.initDialogPath;
                        }
                        Winforms.DialogResult result = dlg.ShowDialog();
                        TextValues.scriptFile = dlg.SelectedPath;
                    }
                }
                break;
            }

            worker.RunWorkerAsync();

        }


















        private void btn_folder_path_Button_Click(object sender, RoutedEventArgs e)
        {
            txtbox_sql_files.Text = "";
            TextValues.sqlFilesFullPathes.Clear();
            TextValues.sprojFilesFullPathes.Clear();
            TextValues.sqlFilesUpdatedFullPathes.Clear();
            TextValues.sqlFilesUpdatedFullPathesNewNames.Clear();
            TextValues.mdtTableNames.Clear();


            using (var dialog = new Winforms.FolderBrowserDialog())
            {
                // путь к папке, в которой будет происходить поиск sql скриптов
                if (Directory.Exists(TextValues.initDialogPath))
                {
                    dialog.SelectedPath = TextValues.initDialogPath;
                }
                Winforms.DialogResult result = dialog.ShowDialog();
                TextValues.folderScanPath = dialog.SelectedPath;
                TextValues.initDialogPath = TextValues.folderScanPath;
                txtbox_folder_path.Text = TextValues.folderScanPath;

                int num = 0;
                foreach (string val in ReturnPath(TextValues.folderScanPath, TextValues.sqlFilesFullPathes, "*.sql"))
                {
                    num++;
                }
                foreach (string val in ReturnPath(TextValues.folderScanPath, TextValues.sprojFilesFullPathes, "*.sqlproj"))
                {
                    num++;
                }

                txtbox_sql_files.Text = $"Будет просмотрено {Convert.ToString(num++)} файлов\n";
            }               
        }











        private void btn_run_Button_Click(object sender, RoutedEventArgs e)
        {
            txtbox_sql_files.Text = string.Empty;

            if (string.IsNullOrWhiteSpace(txtbox_folder_path.Text))
            {
                txtbox_sql_files.Text += "Забыл выбрать папку\n";
                return;
            }

            Refact refact = new Refact(txtbox_folder_path.Text, txtbox_folder_path.Text, txtbox_run_source_name.Text, txtbox_run_target_name.Text, "*.sql");
            refact.CommentInsteadDelete = (chkbox_comment_delete.IsChecked == true) ? true : false;

            switch (cbbx_WhatToDo.Text)
            {
                case "1 Show files containing pattern":
                    refact.ShowFilesContainPatternList(txtbox_sql_files, "namesNoExtension");
                    break;
                case "2.1 Replace existing text by the new one (word or part only)":
                    refact.ReplaceTextByPatternOneWord(1);
                    break;
                case "2.2 Replace full line with existing text by the new one (by pattern)":
                    refact.ReplaceTextByPatternFullLine();
                    break;
                case "2.3 Replace full line with existing text by the new one (full word pattern)":
                    refact.ReplaceTextByWordFullLine();
                    break;
                case "2.4 Replace part of the word by the new one (pattern != oldText, replaced by newText)":
                    refact.ReplaceTextByPatternOneWord(2);
                    break;
                case "3.1 Delete part of word in text (by pattern)":
                    refact.DeleteTextByPatternOneWord();
                    break;
                case "3.2 Delete full line (by pattern)":
                    refact.DeleteTextByPatternFullLine();
                    break;
                case "3.3 Delete full line (by word)":
                    refact.DeleteTextByWordFullLine();
                    break;
                case "4.1 Insert line before line with pattern once":
                    refact.InsertBeforeTextByPatternFullLineOnce();
                    break;
                case "4.2 Insert line before line with pattern all times":
                    refact.InsertBeforeTextByPatternFullLineAll();
                    break;
                case "4.3 Insert line after line with pattern once":
                    refact.InsertAfterTextByPatternFullLineOnce();
                    break;
                case "4.4 Insert line after line with pattern all times":
                    refact.InsertAfterTextByPatternFullLineAll();
                    break;
                case "5 Rename files and change sqlproj":
                    refact.RenameFilesAndSqlproj(null);
                    break;
                default:
                    refact.ShowFilesContainPatternList(txtbox_sql_files, "full");
                    break;
            }           

        }


        






        // преименование таблиц, stage 1
        private void btn_rename_Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtbox_folder_path.Text))
            {
                txtbox_sql_files.Text += "Забыл выбрать папку\n";
                return;
            }

            string folder = TextValues.folderScanPath;

            // создаем папку, куда скопируем оригиналы на всякий случай

            string pathCopyOrig = "";
            if (chkbox_copy.IsChecked == true)
            {
                using (var dialog = new Winforms.FolderBrowserDialog())
                {
                    Winforms.DialogResult result = dialog.ShowDialog();
                    pathCopyOrig = dialog.SelectedPath;
                }               
            }

            int countFilesChanged = 0;

            // ищем изменяемые файлы и заполняем массив соответствий старых и новых файлов
            foreach (string sql in TextValues.sqlFilesFullPathes)
            {
                StreamReader sr = new StreamReader(sql);
                // функция ChangeText заполняет список TextValues.sqlFilesUpdatedFullPathes путями исходных файлов, которые изменятся
                ChangeText_part1(sr.ReadToEnd(), sql);
                sr.Close();
            }

            // заполняем таблицу соответствия путей исходных файлов и переименованных
            foreach (string sql in TextValues.sqlFilesUpdatedFullPathes)
            {
                TextValues.sqlFilesUpdatedFullPathesNewNames.Add(ChangeText_part1(sql, null));

                //// тут можно лог что на что меняем огранизовать
                //txtbox_sql_files.Text += $"{sql} -> {ChangeText(sql, null)}\n";
            }

            // перезаписываем содержимое файлов sql
            foreach (string sql in TextValues.sqlFilesUpdatedFullPathes)
            {
                // читаем
                StreamReader sr = new StreamReader(sql);

                string text = ChangeText_part1(sr.ReadToEnd(), sql);

                sr.Close();

                // копируем файлы перед перезаписью
                if (chkbox_copy.IsChecked == true)
                {
                    if (Directory.Exists(pathCopyOrig))
                    {
                        string sqlCopy = pathCopyOrig + "\\" + sql.Substring(sql.LastIndexOf('\\') + 1, sql.Length - sql.LastIndexOf('\\') - 1);
                        try
                        {
                            File.Copy(sql, sqlCopy, false);
                        }
                        catch { }
                    }
                }

                // перезаписываем
                StreamWriter sw = new StreamWriter(sql);
                sw.Write(text);
                sw.Close();
                
                countFilesChanged++;
            }

            // переименовываем файлы sql
            foreach (string sql in TextValues.sqlFilesUpdatedFullPathes)
            {
                try
                {
                    File.Move(sql, ChangeText_part1(sql, null));
                }
                catch { }
            }

            // перезаписываем содержимое файлов sproj
            foreach (string sproj in TextValues.sprojFilesFullPathes)
            {
                StreamReader sr = new StreamReader(sproj);
                string text = sr.ReadToEnd();
                sr.Close();

                string oldArray = "";
                string newArray = "";
                string oldName = "";
                string newName = "";

                for (int i = 0; i < TextValues.sqlFilesUpdatedFullPathes.Count; i++)
                {
                    oldArray = TextValues.sqlFilesUpdatedFullPathes[i];
                    oldName = oldArray.Substring(oldArray.LastIndexOf('\\') + 1, oldArray.Length - oldArray.LastIndexOf('\\') - 1);

                    newArray = TextValues.sqlFilesUpdatedFullPathesNewNames[i];
                    newName = newArray.Substring(newArray.LastIndexOf('\\') + 1, newArray.Length - newArray.LastIndexOf('\\') - 1);

                    text = text.Replace(oldName, newName);
                }

                StreamWriter sw = new StreamWriter(sproj);
                sw.Write(text);
                sw.Close();

                countFilesChanged++;
            }


            if (countFilesChanged > 0)
            {
                txtbox_sql_files.Text += $"Изменено {countFilesChanged} файлов\n";
            }
            else
            {
                txtbox_sql_files.Text += $"Изменять нечего. Если не так, уточни шаблон.\n";
            }

        }

        // удаление identity
        private void btn_rename_2_Button_Click(object sender, RoutedEventArgs e)
        {
            if (txtbox_folder_path.Text == "")
            {
                txtbox_sql_files.Text += "Забыл выбрать папку\n";
                return;
            }

            int countFilesChanged = 0;

            // перезаписываем содержимое файлов sql
            foreach (string sql in TextValues.sqlFilesFullPathes)
            {
                // читаем
                StreamReader sr = new StreamReader(sql);

                string text = sr.ReadToEnd();

                sr.Close();

                if (text.Contains("ID_MDT_RECORD"))
                {
                    text = DeleteLineTextInSql(text, "ID_MDT_RECORD");
                }

                if (text.Contains("IDENTITY (1, 1) "))
                {
                    text = ReplaceTextInSql(text, "IDENTITY (1, 1) ");
                }

                if (text.Contains("ID_MDT_RECORD") || text.Contains("IDENTITY (1, 1) "))
                {
                    // перезаписываем
                    StreamWriter sw = new StreamWriter(sql);
                    sw.Write(text);
                    sw.Close();

                    countFilesChanged++;
                }
               
            }

            if (countFilesChanged > 0)
            {
                txtbox_sql_files.Text += $"Изменено {countFilesChanged} файлов\n";
            }
            else
            {
                txtbox_sql_files.Text += $"Изменять нечего. Если не так, уточни шаблон.\n";
            }

        }


        // добавление полей в dim
        private void btn_rename_3_Button_Click(object sender, RoutedEventArgs e)
        {
            if (txtbox_folder_path.Text == "")
            {
                txtbox_sql_files.Text += "Забыл выбрать папку\n";
                return;
            }



            if (Directory.Exists(txtbox_compare_path.Text))
            {
                // записываем имена sql файлов в схеме mdt.
                foreach (string val in ReturnPath(txtbox_compare_path.Text, TextValues.mdtTableFullPathes, "*.sql"))
                {
                    StreamReader sr = new StreamReader(val);
                    string text = sr.ReadToEnd();
                    if (text.Contains("ID_Mapping_DataSource") || text.Contains("ID_Mapping_MDT") || text.Contains("UID_DS"))
                    {
                        TextValues.mdtTableNames.Add(val.Substring(val.LastIndexOf('\\') + 5, val.Length - val.LastIndexOf('\\') - 5));
                    }
                }
            }          

            int countFilesChanged = 0;

            // перезаписываем содержимое файлов sql
            foreach (string sql in TextValues.sqlFilesFullPathes)
            {
                // читаем
                StreamReader sr = new StreamReader(sql);
                string text = sr.ReadToEnd();

                if (!text.Contains("MDT_FlagActive"))
                {
                    text = AddLineTextInSql(text, "    [MDT_FlagActive] 	        BIT            NOT NULL,", "CONSTRAINT", "after");
                }
                if (!text.Contains("MDT_DateDeleted"))
                {
                    text = AddLineTextInSql(text, "    [MDT_DateDeleted] 	        DATETIME2(0)   NULL,", "CONSTRAINT", "after");
                }

                sr.Close();
            
                // перезаписываем
                StreamWriter sw = new StreamWriter(sql);
                sw.Write(text);
                sw.Close();
            
                countFilesChanged++;
            }

            // проверяем и выводим названия таблиц с одинаковыми менами чтобы проверить надо ли добавлять поля UID_DS, MDT_FlagActive, MDT_DataDeleted
            foreach (var dimTable in TextValues.sqlFilesFullPathes)
            {
                if (TextValues.mdtTableNames.Contains(dimTable.Substring(dimTable.LastIndexOf('\\') + 5, dimTable.Length - dimTable.LastIndexOf('\\') - 5)))
                {
                    // читаем
                    StreamReader sr = new StreamReader(dimTable);

                    string text = sr.ReadToEnd();
                    if (!text.Contains("ID_Mapping_DataSource"))
                    {
                        text = AddLineTextInSql(text, "    [ID_Mapping_DataSource] 	  INT            NOT NULL,", "CONSTRAINT", "after");
                    }
                    if (!text.Contains("ID_Mapping_MDT"))
                    {
                        text = AddLineTextInSql(text, "    [ID_Mapping_MDT] 	        INT            NULL,", "CONSTRAINT", "after");
                    }
                    if (!text.Contains("UID_DS"))
                    {
                        text = AddLineTextInSql(text, "    [UID_DS] 	                 VARCHAR(255)   NULL,", "CONSTRAINT", "after");
                    }

                    sr.Close();
                    
                    // перезаписываем
                    StreamWriter sw = new StreamWriter(dimTable);
                    sw.Write(text);
                    sw.Close();

                    //txtbox_sql_files.Text += dimTable.Substring(dimTable.LastIndexOf('\\') + 5, dimTable.Length - dimTable.LastIndexOf('\\') - 5) + "\n";

                }
            }

            if (countFilesChanged > 0)
            {
                txtbox_sql_files.Text += $"Изменено {countFilesChanged} файлов\n";
            }
            else
            {
                txtbox_sql_files.Text += $"Изменять нечего. Если не так, уточни шаблон.\n";
            }
        }

        private void btn_rename_4_Button_Click(object sender, RoutedEventArgs e)
        {
            if (txtbox_folder_path.Text == "")
            {
                txtbox_sql_files.Text += "Забыл выбрать папку\n";
                return;
            }

            int countFilesChanged = 0;

            // перезаписываем содержимое файлов sql
            foreach (string sql in TextValues.sqlFilesFullPathes)
            {
                // читаем
                StreamReader sr = new StreamReader(sql);
                string text = sr.ReadToEnd();
                sr.Close();

                // если снята галка - не перезаписывать поля в таблицах
                if (chkbox_change_tables.IsChecked == false)
                {
                    if (!TextContainsPattern(text, "create table"))
                    {
                        if (TextContainsPattern(text, txtbox_field_source.Text))
                        {
                            countFilesChanged++;
                            text = ReplaceTextInSql(text, txtbox_field_source.Text, txtbox_field_target.Text);

                            // перезаписываем
                            StreamWriter sw = new StreamWriter(sql);
                            sw.Write(text);
                            sw.Close();
                        }

                    }
                }
                // если стоит галка перезаписывать поля в таблицах - то не проверяем тест sql скрипта
                else
                {
                    if (TextContainsPattern(text, txtbox_field_source.Text))
                    {
                        countFilesChanged++;
                        text = ReplaceTextInSql(text, txtbox_field_source.Text, txtbox_field_target.Text);

                        // перезаписываем
                        StreamWriter sw = new StreamWriter(sql);
                        sw.Write(text);
                        sw.Close();
                    }                
                }

                
            }

            if (countFilesChanged > 0)
            {
                txtbox_sql_files.Text += $"Изменено {countFilesChanged} файлов\n";
            }
            else
            {
                txtbox_sql_files.Text += $"Изменять нечего. Если не так, уточни шаблон.\n";
            }
        }


        private void btn_rename_5_Button_Click(object sender, RoutedEventArgs e)
        {
            if (txtbox_folder_path.Text == "")
            {
                txtbox_sql_files.Text += "Забыл выбрать папку\n";
                return;
            }

            int countFilesChanged = 0;

            foreach (string sql in TextValues.sqlFilesFullPathes)
            {
                // читаем
                StreamReader sr = new StreamReader(sql);
                string text = sr.ReadToEnd();
                sr.Close();

                if (TextContainsPattern(text, "relation."))
                {
                    // меняем текст
                    text = ReplaceTextInSql(text, "relation.R_", "dim.");                   
                    text = ReplaceTextWholeWordInSql(text, "Type_Source", "dst.Code");
                    //text = AddLineTextInSql(text, "cross join dim.DataSourceType as dst", "from", "after");

                    countFilesChanged++;

                    // перезаписываем
                    StreamWriter sw = new StreamWriter(sql);
                    sw.Write(text);
                    sw.Close();

                }


            }

            if (countFilesChanged > 0)
            {
                txtbox_sql_files.Text += $"Изменено {countFilesChanged} файлов\n";
            }
            else
            {
                txtbox_sql_files.Text += $"Изменять нечего. Если не так, уточни шаблон.\n";
            }

        }



      















        // переопределяем текст
        public static string ChangeText_part1(string text, string path)
        {
            bool changedFile = false;

            //if (TextContainsPattern(text, "sa.SA_"))
            //{
            //    text = text.Replace("sa.SA_", "sa.").Replace("SA.SA_", "sa.").Replace("[sa].[SA_", "[sa].[").Replace("[SA].[SA_", "[sa].[");
            //    changedFile = true;
            //}

            if (TextContainsPattern(text, "sa.SA_"))
            {
                text = ReplaceTextAllPattern(text, "sa.SA_", "[sa].[");
                changedFile = true;
            }

            if (TextContainsPattern(text, "ha.VIEW_H_"))
            {
                text = text.Replace("ha.VIEW_H_", "ha.vw_").Replace("[ha].[VIEW_H_", "[ha].[vw_");
                changedFile = true;
            }

            if (TextContainsPattern(text, "dbo.VIEW_SA_"))
            {
                text = text.Replace("dbo.VIEW_SA_", "mdt.vw_").Replace("[dbo].[VIEW_SA_", "[mdt].[vw_");
                changedFile = true;
            }

            if (TextContainsPattern(text, "dbo.VIEW_D_"))
            {
                text = text.Replace("dbo.VIEW_D_", "dim.vw_").Replace("[dbo].[VIEW_D_", "[dim].[vw_");
                changedFile = true;
            }

            if (TextContainsPattern(text, "dbo.VIEW_F_"))
            {
                text = text.Replace("dbo.VIEW_F_", "fact.vw_").Replace("[dbo].[VIEW_F_", "[fact].[vw_");
                changedFile = true;
            }

            if (TextContainsPattern(text, "dbo.VIEW_OLAP_"))
            {
                text = text.Replace("dbo.VIEW_OLAP_", "olap.vw_").Replace("[dbo].[VIEW_OLAP_", "[olap].[vw_");
                changedFile = true;
            }

            if (TextContainsPattern(text, "View_OLAP_"))
            {
                text = text.Replace("VIEW_OLAP_", "olap.vw_").Replace("[VIEW_OLAP_", "[olap].[vw_");
                changedFile = true;
            }

            if (TextContainsPattern(text, "dbo.FN_F_"))
            {
                text = text.Replace("dbo.FN_F_", "fact.fn_").Replace("[dbo].[FN_F_", "[fact].[fn_");
                changedFile = true;
            }


            // меняем только ключи, если в тексте есть слова с ключами, разбиваем весь текст на слова и просматриваем каждое слово. Слова, начинающиеся с PKC_ и т.п. модифицируем и меняем в тексте
            if (text.Contains("PKC_") || text.Contains("PK_") || text.Contains("FK_") || text.Contains("UC_") || text.Contains("UK_"))
            {
                string[] words = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string word in words)
                {
                    if (Regex.IsMatch(word, "^PKC_") || Regex.IsMatch(word, @"^\[PKC_") || Regex.IsMatch(word, "^PK_") || Regex.IsMatch(word, @"^\[PK_") || Regex.IsMatch(word, "^FK_") || Regex.IsMatch(word, @"^\[FK_") || Regex.IsMatch(word, "^UC_") || Regex.IsMatch(word, @"^\[UC_") || Regex.IsMatch(word, "^UK_") || Regex.IsMatch(word, @"^\[UK_"))
                    {
                        text = text.Replace(word, word.Replace("_H_", "_").Replace("_F_", "_").Replace("_D_", "_"));
                        changedFile = true;
                    }
                }               
            }

            
            if (TextContainsPattern(text, "dbo.D_"))
            {
                text = text.Replace("dbo.D_", "dim.").Replace("[dbo].[D_", "[dim].[").Replace("dbo.[D_", "[dim].[");
                changedFile = true;
            }

            if (TextContainsPattern(text, "ha.H_"))
            {
                text = text.Replace("ha.H_", "ha.").Replace("[ha].[H_", "[ha].[").Replace("ha.[H_", "[ha].[");
                changedFile = true;
            }

            if (TextContainsPattern(text, "dbo.F_"))
            {
                text = text.Replace("dbo.F_", "fact.").Replace("[dbo].[F_", "[fact].[").Replace("dbo.[F_", "[fact].[");
                changedFile = true;
            }

            if (TextContainsPattern(text, "View_"))
            {
                string[] words = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string word in words)
                {
                    if (Regex.IsMatch(word, "^D_"))
                    {
                        text = text.Replace(word, word.Replace("D_", "dim."));
                        // запоминая word по шаблону (начиная с D_) проверки на начало слова с D_ при replace уже не выполняются и меняется ID_xxx -> Idim.xxx
                        text = text.Replace("Idim.", "ID_");
                        changedFile = true;
                    }
                    if (Regex.IsMatch(word, "^F_"))
                    {
                        text = text.Replace(word, word.Replace("F_", "fact."));
                        changedFile = true;
                    }
                }
            }

            if (TextContainsPattern(text, "fact.vw_"))
            {
                string[] words = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string word in words)
                {
                    if (Regex.IsMatch(word, "^D_"))
                    {
                        text = text.Replace(word, word.Replace("D_", "dim."));
                        text = text.Replace("Idim.", "ID_");
                        changedFile = true;
                    }
                    if (Regex.IsMatch(word, "^F_"))
                    {
                        text = text.Replace(word, word.Replace("F_", "fact."));
                        changedFile = true;
                    }
                }
            }

            // если в файле есть изменения, путь к нему будет записан в список
            if (changedFile && path != null)
            {
                if (!TextValues.sqlFilesUpdatedFullPathes.Contains(path))
                {
                    TextValues.sqlFilesUpdatedFullPathes.Add(path);
                }
                
            }

            return text;
        }


        // удаляем часть строки по шаблону
        public static string ReplaceTextInSql(string sqlText, string sqlDeleteText)
        {
            if (TextContainsPattern(sqlText, sqlDeleteText))
            {
                sqlText = sqlText.Replace(sqlDeleteText, "").Replace(sqlDeleteText.ToUpper(), "").Replace(sqlDeleteText.ToLower(), "");
            }

            return sqlText;
        }

        // заменяем часть строки по шаблону
        public static string ReplaceTextInSql(string sqlText, string sqlDeleteText, string sqlReplaceText)
        {
            if (TextContainsPattern(sqlText, sqlDeleteText))
            {
                sqlText = sqlText.Replace(sqlDeleteText, sqlReplaceText).Replace(sqlDeleteText.ToUpper(), sqlReplaceText).Replace(sqlDeleteText.ToLower(), sqlReplaceText);
            }

            return sqlText;
        }

        // заменяем часть строки целиком слово, содержащее шаблон
        public static string ReplaceTextWholeWordInSql(string sqlText, string sqlDeleteText, string sqlReplaceText)
        {
            // удаляем ID_MDT_Record
            if (TextContainsPattern(sqlText, sqlDeleteText))
            {
                List<string> newText = new List<string>();
                string[] words = sqlText.Split(new char[] { ' ' });
                foreach (string word in words)
                {
                    
                    if (!TextContainsPattern(word, sqlDeleteText))
                    {
                        newText.Add(word);
                    }
                    else
                    {
                        // иногда в студии в select поля перечислены переносом строки без пробела и чтобы не затереть вообще всю запись разобьем еще и \n каждое слово
                        string[] wordsEnter = word.Split(new char[] { '\n' });
                        List<string> newTextEnter = new List<string>();
                        foreach (string wordEnter in wordsEnter)
                        {
                            if (!TextContainsPattern(wordEnter, sqlDeleteText))
                            {
                                newTextEnter.Add(wordEnter);
                            }
                            else
                            {
                                string sqlReplaceTextMod = sqlReplaceText;
                                // проверим запятую перед словом, чтобы добавить, если она была (для селекта)
                                if (wordEnter.Trim().Substring(0,1) == ",")
                                {
                                    sqlReplaceTextMod = "," + sqlReplaceText;
                                }
                                int numTabs = 0;

                                // считаем количество табов в начале строки (так же пробелы учитываются)
                                foreach (char c in wordEnter)
                                {
                                    if (c == '\t')
                                    {
                                        numTabs++;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                if (numTabs != 0)
                                {
                                    for (int i = 0; i < numTabs; i++)
                                    {
                                        sqlReplaceTextMod = "\t" + sqlReplaceTextMod;
                                    }
                                }

                                newTextEnter.Add(wordEnter.Replace(wordEnter, sqlReplaceTextMod).Replace(wordEnter.ToUpper(), sqlReplaceTextMod).Replace(wordEnter.ToLower(), sqlReplaceTextMod));
                            }
                        }

                        newText.Add(string.Join("\n", newTextEnter));
                    }
                }
                sqlText = string.Join(" ", newText);
            }

            return sqlText;
        }

        // удаляем поля из текста sql скрипта, указываем какую строку удалять по паттерну
        public static string DeleteLineTextInSql(string sqlText, string sqlDeleteLineText)
        {
            // удаляем ID_MDT_Record
            if (TextContainsPattern(sqlText, sqlDeleteLineText))
            {
                List<string> newText = new List<string>();
                string[] lines = sqlText.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string line in lines)
                {
                    if (!TextContainsPattern(line, sqlDeleteLineText))
                    {
                        newText.Add(line);
                    }
                }
                sqlText = string.Join("\n", newText);                            
            }

            return sqlText;
        }

        // добавляем поля в текст sql скрипта, указываем перед какой строкой вставить (строкой, содержащей слово или группу слов)
        public static string AddLineTextInSql(string sqlText, string sqlAddLineText, string beforeLineContainsWord, string before_after)
        {
            List<string> newText = new List<string>();
            int position = 0;
            string[] lines = sqlText.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
            {
                // определяет будет ли внесена строка до искомой beforeLineContainsWord или после
                if (before_after == "after")
                {
                    newText.Add(line);
                    position++;
                }

                if (TextContainsPattern(line, beforeLineContainsWord))
                {
                    break;
                }

                // определяет будет ли внесена строка до искомой beforeLineContainsWord или после
                if (before_after == "before")
                {
                    newText.Add(line);
                    position++;
                }

            }          
            newText.Add(sqlAddLineText);
            for (int i = position; i < lines.Length; i++)
            {
                newText.Add(lines[i]);
            }

            sqlText = string.Join("\n", newText);

            return sqlText;
        }





        public static bool TextContainsPattern(string text, string replacingText)
        {
            string replacingText0 = replacingText;
            string replacingText1 = replacingText0.ToUpper();
            string replacingText2 = replacingText0.ToLower();
            if (replacingText.Contains("[") || replacingText.Contains("]"))
            {
                replacingText0 = replacingText.Replace("[", "").Replace("]", "");
            }
            string replacingText3 = replacingText0.ToUpper();
            string replacingText4 = replacingText0.ToLower();

            if (text.Contains(replacingText0) || text.Contains(replacingText1) || text.Contains(replacingText2) || text.Contains(replacingText3) || text.Contains(replacingText4))
            {
                return true;
            }

            if (replacingText.Contains('.'))
            {
                string[] replacingwords = replacingText0.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (replacingwords.Length > 1)
                {
                    string replacingText5 = "[" + replacingwords[0] + "]" + "." + "[" + replacingwords[1];
                    string replacingText6 = replacingText5.ToUpper();
                    string replacingText7 = replacingText5.ToLower();
                    string replacingText8 = "[" + replacingwords[0].ToUpper() + "]" + "." + "[" + replacingwords[1].ToLower();
                    string replacingText9 = "[" + replacingwords[0].ToLower() + "]" + "." + "[" + replacingwords[1].ToUpper();
                    string replacingText10 = replacingwords[0] + "[" + replacingwords[1];
                    string replacingText11 = replacingText10.ToUpper();
                    string replacingText12 = replacingText10.ToLower();
                    string replacingText13 = replacingwords[0].ToUpper() + "[" + "." + replacingwords[1].ToLower();
                    string replacingText14 = replacingwords[0].ToLower() + "[" + "." + replacingwords[1].ToUpper();
                    string replacingText15 = "[" + replacingwords[0] + "]" + "." + replacingwords[1];
                    string replacingText16 = replacingText15.ToUpper();
                    string replacingText17 = replacingText15.ToLower();
                    string replacingText18 = "[" + replacingwords[0].ToUpper() + "]" + "." + replacingwords[1].ToLower();
                    string replacingText19 = "[" + replacingwords[0].ToLower() + "]" + "." + replacingwords[1].ToUpper();

                    if (text.Contains(replacingText5)  || text.Contains(replacingText6)  || text.Contains(replacingText7)  || text.Contains(replacingText8)  || 
                        text.Contains(replacingText9)  || text.Contains(replacingText10) || text.Contains(replacingText11) || text.Contains(replacingText12) ||
                        text.Contains(replacingText13) || text.Contains(replacingText14) || text.Contains(replacingText15) || text.Contains(replacingText16) ||
                        text.Contains(replacingText17) || text.Contains(replacingText18) || text.Contains(replacingText19))
                    {
                        return true;
                    }
                }
                else
                {
                    string replacingText5 = "[" + replacingwords[0] + "]" + ".";
                    string replacingText6 = replacingText5.ToUpper();
                    string replacingText7 = replacingText5.ToLower();

                    if (text.Contains(replacingText5) || text.Contains(replacingText6) || text.Contains(replacingText7))
                    {
                        return true;
                    }
                }

            }

            return false;
        }









        // передаем текст для для замены без [], пытается заменить текст в любой конфигурации: dbo.D_ , DBO.D_ , dbo.d_ , [dbo].[D_ , dbo.[D_ , [dbo].D_ (+ все большими или маленькими буквами)
        public static string ReplaceTextAllPattern(string text, string replacingText, string replacementText)
        {
            // тупо убираем скобки, т.к. с ними очень много гемора
            if (replacementText.Contains("[") || replacementText.Contains("]"))
            {
                replacementText = replacementText.Replace("[", "").Replace("]", "");
            }

            // поиск всевозможных вариантов замены в тексте
            string replacingText0 = replacingText;
            string replacingText1 = replacingText0.ToUpper();
            string replacingText2 = replacingText0.ToLower();
            if (replacingText.Contains("[") || replacingText.Contains("]"))
            {
                replacingText0 = replacingText.Replace("[", "").Replace("]", "");
            }
            string replacingText3 = replacingText0.ToUpper();
            string replacingText4 = replacingText0.ToLower();

            if (replacingText.Contains('.'))
            {
                string[] replacingwords = replacingText0.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (replacingwords.Length > 1)
                {
                    string replacingText5 = "[" + replacingwords[0] + "]" + "." + "[" + replacingwords[1];
                    string replacingText6 = replacingText5.ToUpper();
                    string replacingText7 = replacingText5.ToLower();
                    string replacingText8 = "[" + replacingwords[0].ToUpper() + "]" + "." + "[" + replacingwords[1].ToLower();
                    string replacingText9 = "[" + replacingwords[0].ToLower() + "]" + "." + "[" + replacingwords[1].ToUpper();
                    string replacingText10 = replacingwords[0] + "[" + replacingwords[1];
                    string replacingText11 = replacingText10.ToUpper();
                    string replacingText12 = replacingText10.ToLower();
                    string replacingText13 = replacingwords[0].ToUpper() + "[" + "." + replacingwords[1].ToLower();
                    string replacingText14 = replacingwords[0].ToLower() + "[" + "." + replacingwords[1].ToUpper();
                    string replacingText15 = "[" + replacingwords[0] + "]" + replacingwords[1];
                    string replacingText16 = replacingText15.ToUpper();
                    string replacingText17 = replacingText15.ToLower();
                    string replacingText18 = "[" + replacingwords[0].ToUpper() + "]" + "." + replacingwords[1].ToLower();
                    string replacingText19 = "[" + replacingwords[0].ToLower() + "]" + "." + replacingwords[1].ToUpper();

                    text = text.Replace(replacingText0, replacementText).Replace(replacingText1, replacementText).Replace(replacingText2,
                                                        replacementText).Replace(replacingText3, replacementText).Replace(replacingText4, replacementText).Replace(replacingText5,
                                                        replacementText).Replace(replacingText6, replacementText).Replace(replacingText7, replacementText).Replace(replacingText8,
                                                        replacementText).Replace(replacingText9, replacementText).Replace(replacingText10, replacementText).Replace(replacingText11,
                                                        replacementText).Replace(replacingText12, replacementText).Replace(replacingText13, replacementText).Replace(replacingText14,
                                                        replacementText).Replace(replacingText15, replacementText).Replace(replacingText16, replacementText).Replace(replacingText17,
                                                        replacementText).Replace(replacingText18, replacementText).Replace(replacingText19, replacementText);
                }
                else
                {
                    string replacingText5 = "[" + replacingwords[0] + "]" + ".";
                    string replacingText6 = replacingText5.ToUpper();
                    string replacingText7 = replacingText5.ToLower();

                    text = text.Replace(replacingText0, replacementText).Replace(replacingText1, replacementText).Replace(replacingText2,
                                                        replacementText).Replace(replacingText3, replacementText).Replace(replacingText4, replacementText).Replace(replacingText5,
                                                        replacementText).Replace(replacingText6, replacementText).Replace(replacingText7, replacementText);
                }
                
            }
            else
            {
                text = text.Replace(replacingText0, replacementText).Replace(replacingText1, replacementText).Replace(replacingText2, replacementText).Replace(replacingText3, replacementText).Replace(replacingText4, replacementText);
            }

            // до лучших времен, замена текста с подстановкой [ и ] дюже геморна из-за того что в тексте sql скрипта может быть много вариантов
            // если в заменяемой части слова нет [ или ], то нужно из всего слова удалить закрывающие ковычки 
            if (!replacementText.Contains("[") && !replacementText.Contains("]"))
            {
                List<string> newText = new List<string>();
                string[] words = text.Split(new char[] { ' ' });
                foreach (string word in words)
                {
                    if (!TextContainsPattern(word, replacementText))
                    {
                        newText.Add(word);
                    }
                    else
                    {
                        newText.Add(word.Replace("[", "").Replace("]", ""));
                    }
                }
                text = string.Join(" ", newText);
            }
            else
            {
                List<string> newText = new List<string>();
                string[] words = text.Split(new char[] { ' ' });
                foreach (string word in words)
                {
                    if (!TextContainsPattern(word, replacementText))
                    {
                        newText.Add(word);
                    }
                    else
                    {
                        if (word.ToCharArray().Last() != ']')
                        {
                            newText.Add(word + "]");
                        }
                        else
                        {
                            newText.Add(word);
                        }
                        
                    }
                }
                text = string.Join(" ", newText);
            }

            return text;
        }







        // ищем файлы в выбранной папке по паттерну
        public static List<string> ReturnPath(string directory, List<string> outList, string pattern)
        {
            foreach (var file in Directory.GetFiles(directory, pattern, SearchOption.AllDirectories))
            {
                outList.Add(file);
            }

            return outList;
        }


    }

    public class TextValues
    {
        // путь к папке, в которой откроется выбор папки
        public static string initDialogPath = "";
        // путь к папке, в которой будут искаться файлы
        public static string folderScanPath = "";
        // список полных путей к файлам .sql
        public static List<string> sqlFilesFullPathes = new List<string>();
        // список полных путей к файлам .sproj
        public static List<string> sprojFilesFullPathes = new List<string>();
        // список полных путей к файлам .sql которые изменились
        public static List<string> sqlFilesUpdatedFullPathes = new List<string>();
        // список полных путей к файлам .sql c новыми именами 
        public static List<string> sqlFilesUpdatedFullPathesNewNames = new List<string>();
        // список полных путей к файлам .sql к таблицам в схеме mdt 
        public static List<string> mdtTableFullPathes = new List<string>();
        // список названий таблиц в схеме mdt для сравнения
        public static List<string> mdtTableNames = new List<string>();
        public static string refactFile = "";
        public static string scriptFile = "";
        // список путей к временным файлам
        public static List<string> pathesToTempFiles = new List<string>();

    }
}

